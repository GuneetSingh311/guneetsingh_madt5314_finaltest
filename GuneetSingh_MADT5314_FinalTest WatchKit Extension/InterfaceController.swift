//
//  InterfaceController.swift
//  GuneetSingh_MADT5314_FinalTest WatchKit Extension
//
//  Created by Guneet on 2019-03-14.
//  Copyright © 2019 Guneet. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    // MARK: Variables
    
   let suggestionsList = ["Emad", "Guneet", "Jake"]
    var text = "EnterName"
    
    //MARK: Outlets.
    
    @IBOutlet weak var nameLabel: WKInterfaceLabel!
    @IBOutlet weak var startGameButton: WKInterfaceButton!
    
    @IBAction func startButtonAction() {
        
       
    }
    
    
    @IBAction func enterName() {
  
        presentTextInputController(withSuggestions: suggestionsList, allowedInputMode: .plain) { (results) in
            
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                self.nameLabel.setText(userResponse)
                self.text = userResponse!
                UserDefaults.standard.set(self.text, forKey:"username")
                UserDefaults.standard.set(self.text, forKey: "name")
            }
        }

    }
    
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        let name = UserDefaults.standard.value(forKey: "name") as? String
        
        if name == "" {
            self.nameLabel.setText("EnterName")
        }
        else {
        self.nameLabel.setText(name)
         presentController(withName: "start", context: nil)
        }
        
    
        }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    

    

    
    
    

}
