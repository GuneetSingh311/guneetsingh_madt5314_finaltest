//
//  startController.swift
//  GuneetSingh_MADT5314_FinalTest WatchKit Extension
//
//  Created by Guneet on 2019-03-14.
//  Copyright © 2019 Guneet. All rights reserved.
//

import WatchKit
import Foundation

class startController: WKInterfaceController {
    
    var name = ""
    var level = "Easy"
    @IBOutlet weak var nameLabel: WKInterfaceLabel!
    
    @IBAction func easyLevel() {
        level = "Easy"
        UserDefaults.standard.set(level, forKey: "gameValue1")
        print(level)
        presentController(withName: "GameController", context: nil)
        
    
    }
    @IBAction func hardLevel() {
    self.level = "Hard"
    UserDefaults.standard.set(level, forKey: "gameValue1")
        print(level)
    presentController(withName: "GameController", context: nil)
    }
    
    
    
    
    
    
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        self.name = UserDefaults.standard.value(forKey: "username") as! String
        self.nameLabel.setText(self.name)
        
        
        
        
        
        
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
