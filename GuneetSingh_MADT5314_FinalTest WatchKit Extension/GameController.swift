//
//  GameController.swift
//  GuneetSingh_MADT5314_FinalTest WatchKit Extension
//
//  Created by Guneet on 2019-03-14.
//  Copyright © 2019 Guneet. All rights reserved.
//

import WatchKit
import Foundation


class GameController: WKInterfaceController {

    // MARK: variables
    var gameLevel:String?
    var imageAName =  "charmender.png"
    var imageBName =  "jiggly.png"
    var imageCName =   "meow.png"
    var imageDName =  "pikachu.png"
    
    
    // MARK: outlets
    
    // MarK Image outlets
    
    @IBOutlet weak var imageA: WKInterfaceImage!
    @IBOutlet weak var imageB: WKInterfaceImage!
    @IBOutlet weak var imageC: WKInterfaceImage!
    @IBOutlet weak var imageD: WKInterfaceImage!
    
    
    // MARK: Button Outlets.
    @IBOutlet weak var image1: WKInterfaceButton!
    @IBOutlet weak var image2: WKInterfaceButton!
    @IBOutlet weak var image3: WKInterfaceButton!
    @IBOutlet weak var image4: WKInterfaceButton!
    
    // MARK: Lable
    @IBOutlet weak var levelLabel: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
     
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        
        super.willActivate()
        
        self.gameLevel = UserDefaults.standard.value(forKey: "gameValue1")as? String
            print(self.gameLevel!)
        
        
        if(self.gameLevel == "Easy")
        {
            self.levelLabel.setText("Easy")
            self.imageA.setImage(UIImage(named:"charmender.png"))
            self.imageB.setImage(UIImage(named:"jiggly.png"))
            self.imageC.setImage(UIImage(named:"meow.png"))
            self.imageD.setImage(UIImage(named:"pikachu.png"))
            
         
            Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false) { (Timer) in
                self.imageA.setImage(UIImage(named:""))
                self.imageB.setImage(UIImage(named:""))
                self.imageC.setImage(UIImage(named:""))
                self.imageD.setImage(UIImage(named:""))
               
                
                
                // MARK: Set images on Buttons
                self.image1.setBackgroundImage(UIImage(named: "meow.png" ))
                self.image2.setBackgroundImage(UIImage(named: "jiggly.png"))
                self.image3.setBackgroundImage(UIImage(named: "pikachu.png"))
                self.image4.setBackgroundImage(UIImage(named: "charmender.png"))
                self.imageAName =  "meow.png"
                self.imageBName =  "jiggly.png"
                self.imageCName =  "pikachu.png"
                self.imageDName =  "charmender.png"

            }
            
            }
            
            
        
        else if(self.gameLevel == "Hard")
        {
            self.levelLabel.setText("Hard")
            self.imageB.setImage(UIImage(named:"charmender.png"))
            self.imageA.setImage(UIImage(named:"jiggly.png"))
            self.imageD.setImage(UIImage(named:"meow.png"))
            self.imageC.setImage(UIImage(named:"pikachu.png"))
           
        
        
        
        
    Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) {
        (Timer) in
            
            self.imageB.setImage(UIImage(named:""))
            self.imageA.setImage(UIImage(named:""))
            self.imageD.setImage(UIImage(named:""))
            self.imageC.setImage(UIImage(named:""))
        
        // MARK: Set images on Buttons
            self.image1.setBackgroundImage(UIImage(named: "jiggly.png" ))
            self.image2.setBackgroundImage(UIImage(named: "meow.png"))
            self.image3.setBackgroundImage(UIImage(named: "pikachu.png"))
            self.image4.setBackgroundImage(UIImage(named: "charmender.png"))
        
        // set the values of button equal to variable
        self.imageAName =  "jiggly.png"
        self.imageBName =   "meow.png"
        self.imageCName =   "pikachu.png"
        self.imageDName =  "charmender.png"
        
      }
}
}
    
    
    
    @IBAction func button1() {
        self.imageA.setImage(UIImage(named: imageAName))
        
}
        
    
    
    @IBAction func button2() {
    
        self.imageB.setImage(UIImage(named: imageBName))
    
    }
    
    
    @IBAction func button3() {
       self.imageC.setImage(UIImage(named: imageCName))
    
    }
    
    
    @IBAction func button4() {
        self.imageD.setImage(UIImage(named: imageDName))
        self.winLose()
        
    
    }
    
    func winLose() {
       
         if(self.gameLevel == "Easy")
        {
        if((imageAName == "meow.png" && imageBName == "jiggly.png") && (imageCName == "pikachu.png" && imageDName == "charmender.png"))
        {
            print("YOU WIN")
            self.levelLabel.setText("You Win")
            
        }
        }
       
         else if(self.gameLevel == "Hard") {
            if((imageAName ==  "jiggly.png" && imageBName == "meow.png") && (imageCName == "pikachu.png" && imageDName == "charmender.png"))
            {
               self.levelLabel.setText("You Win")
                
            }
            
            else {
                print("You lose")
                 self.levelLabel.setText("You Lose")
            }
            
        }
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
